<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%currency_equivalent}}".
 *
 * @property int $currency_equivalent_id
 * @property string|null $currency_name
 * @property string|null $currency_code
 * @property float|null $amount
 * @property float|null $usd_equivalent
 */
class CurrencyEquivalent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%currency_equivalent}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount', 'usd_equivalent'], 'number'],
            [['currency_name'], 'string', 'max' => 100],
            [['currency_code'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'currency_equivalent_id' => 'Currency Equivalent ID',
            'currency_name' => 'Currency Name',
            'currency_code' => 'Currency Code',
            'amount' => 'Amount',
            'usd_equivalent' => 'Usd Equivalent',
        ];
    }
}
