<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%reward_point}}".
 *
 * @property int $reward_point_id
 * @property int $customer_id
 * @property float|null $reward_point
 * @property float|null $equivalent_point
 * @property string|null $datetime
 * @property string|null $status
 * @property int|null $order_id
 */
class RewardPoint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%reward_point}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'order_id'], 'integer'],
            [['reward_point', 'equivalent_point'], 'number'],
            [['datetime'], 'safe'],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reward_point_id' => 'Reward Point ID',
            'customer_id' => 'Customer ID',
            'reward_point' => 'Reward Point',
            'equivalent_point' => 'Equivalent Point',
            'datetime' => 'Datetime',
            'status' => 'Status',
            'order_id' => 'Order ID',
        ];
    }
	
	//createRewardPoint
	public function createRewardPoint($customer_id,$reward_point){
		$model = new RewardPoint();
		$model->customer_id = $customer_id;
		$model->reward_point = $reward_point;
		$model->equivalent_point = $reward_point*0.01; //every 1 point equivalent to USD $0.01
		$model->datetime = date('Y-m-d H:i:s');
		$model->status = 'Collected'; //Collected, Used
		$model->save();
	}

	//getRewardPoint 
	public function getRewardPoint($customer_id,$basket_total){
		$datas = RewardPoint::find()->where(['customer_id'=>$customer_id,'status'=>'Collected'])->all();
		$rewardpoint_amount = 0;
		foreach($datas as $data){
			$rewardpoint_amount = $rewardpoint_amount+$data->equivalent_point;
			if($basket_total>=$rewardpoint_amount){
				$data->status = 'PayProcess';
				$data->save();
			}
		}
		return $rewardpoint_amount;
	}
	//update Reward Point status
	public function updateRewardPoint($customer_id,$order_id,$status){
		$datas = RewardPoint::find()->where(['customer_id'=>$customer_id,'status'=>'PayProcess'])->all();
		foreach($datas as $data){
				$data->status = $status;
				$data->order_id = $order_id;
				$data->save();
		}
	}

}
