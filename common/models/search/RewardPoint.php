<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RewardPoint as RewardPointModel;

/**
 * RewardPoint represents the model behind the search form of `common\models\RewardPoint`.
 */
class RewardPoint extends RewardPointModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reward_point_id', 'customer_id', 'order_id'], 'integer'],
            [['reward_point', 'equivalent_point'], 'number'],
            [['datetime', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RewardPointModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reward_point_id' => $this->reward_point_id,
            'customer_id' => $this->customer_id,
            'reward_point' => $this->reward_point,
            'equivalent_point' => $this->equivalent_point,
            'datetime' => $this->datetime,
            'order_id' => $this->order_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
