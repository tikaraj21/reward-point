<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders as OrdersModel;

/**
 * Orders represents the model behind the search form of `common\models\Orders`.
 */
class Orders extends OrdersModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Order_ID', 'customer_id'], 'integer'],
            [['Order_Date', 'Sales_Type', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdersModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Order_ID' => $this->Order_ID,
            'customer_id' => $this->customer_id,
            'Order_Date' => $this->Order_Date,
        ]);

        $query->andFilterWhere(['like', 'Sales_Type', $this->Sales_Type])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
