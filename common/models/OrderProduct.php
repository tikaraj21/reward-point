<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_product}}".
 *
 * @property int $Order_Product_ID
 * @property int $Order_ID
 * @property string|null $Item_Name
 * @property float|null $Normal_Price
 * @property float|null $Promotion_Price
 *
 * @property Orders $order
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Order_ID'], 'required'],
            [['Order_ID'], 'integer'],
            [['Normal_Price', 'Promotion_Price'], 'number'],
            [['Item_Name'], 'string', 'max' => 155],
            [['Order_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['Order_ID' => 'Order_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Order_Product_ID' => 'Order  Product  ID',
            'Order_ID' => 'Order  ID',
            'Item_Name' => 'Item  Name',
            'Normal_Price' => 'Normal  Price',
            'Promotion_Price' => 'Promotion  Price',
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['Order_ID' => 'Order_ID']);
    }
}
