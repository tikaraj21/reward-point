<?php

namespace common\models;

use Yii;
use common\models\CurrencyEquivalent;

/**
 * This is the model class for table "{{%setting}}".
 *
 * @property int $setting_id
 * @property string|null $title
 * @property string $code
 * @property string|null $value
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%setting}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['title'], 'string', 'max' => 155],
            [['code', 'value'], 'string', 'max' => 100],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'title' => 'Title',
            'code' => 'Code',
            'value' => 'Value',
        ];
    }
	
	//add days in datetime
	public function orderExpiryDate($created_datetime){
		$datetime = date('Y-m-d H:i:s', strtotime('+365 day', $created_datetime)); //add 365days or 1year
		return $datetime;
	}

	//USD converter
	public function usdConverter($currency){
		//free api https://free.currconv.com/api/v7/convert?apiKey=do-not-use-this-api-key-49Spmmb_wsYo4zSaqKiIi&q=NPR_USD&compact=y
		$currency_equivalent = CurrencyEquivalent::find()->where(['currency_code'=>$currency])->one(); //form database or use api 
		return $currency_equivalent->usd_equivalent;
	}
}
