<?php

namespace common\models;

use Yii;
use common\models\Setting;
/**
 * This is the model class for table "{{%orders}}".
 *
 * @property int $Order_ID
 * @property int|null $customer_id
 * @property string|null $Order_Date
 * @property string|null $Sales_Type
 * @property string|null $status
 *
 * @property OrderProduct[] $orderProducts
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['Order_Date'], 'safe'],
            [['Sales_Type', 'status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Order_ID' => 'Order  ID',
            'customer_id' => 'Customer ID',
            'Order_Date' => 'Order  Date',
            'Sales_Type' => 'Sales  Type',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[OrderProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['Order_ID' => 'Order_ID']);
    }
	
	//calculate Total
	public function calculateTotal(){
		$order_products = OrderProduct::find()->where(['Order_ID'=>$this->Order_ID])->all();
		$total = 0;
		foreach($order_products as $order_product){
			if($this->Sales_Type=='Normal'){
				$total = $total+$order_product->Normal_Price;
			}elseif($this->Sales_Type=='Promotion'){
				$total = $total+$order_product->Promotion_Price;
			}else{
				$total = $total+0;
			}
		}
		return $total;
	}
	
	//calculate reward point
	public function rewardCalculation($customer_id){
		$orders = Order::find()->where(['customer_id'=>$customer_id])->all();
		$total_reward_point = 0;
		foreach($orders as $order){
			$setting = Setting::find()->where(['code'=>'CURRENCY'])->one();
			$order_expiry_date = Setting::orderExpiryDate($order->created_datetime);
			$current_datetime = date('Y-m-d H:i:s');

			if($order->status=='Complete' && strtotime($order_expiry_date)>strtotime($current_datetime)){
				$sales_amount = $order->calculateTotal;
				if($sales_amount >=1 && $setting->value=='USD'){
					$reward_point = $sales_amount*1; //$1 sales amount will be rewarded with 1 point
				}elseif($sales_amount >=1 && $setting->value!='USD'){
					$usd_converter = Setting::usdConverter($setting->value);
					$sales_amount_new = $usd_converter*$sales_amount;
					$reward_point = $sales_amount_new*1; //$1 sales amount will be rewarded with 1 point
				}else{
					$reward_point = 0;
				}
				
				$total_reward_point = $total_reward_point+$reward_point;
			}
		}
		return $total_reward_point;
	}
	
	//basket total
	public function basketTotal()
	{
		//session data
		$basket_total = 100; //example
	}
}
