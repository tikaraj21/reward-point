<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Reward Point Calculation!</h1>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-8">
                <h2>Flow Chart</h2>
				<img src="<?=Yii::$app->request->baseUrl;?>/images/flow-chart.png" width="100%"/>
            </div>
            <div class="col-lg-4">
                <h2>ER Diagram</h2>

                <img src="<?=Yii::$app->request->baseUrl;?>/images/ER-diagram.png" class="img-responsive"/>
            </div>
        </div>

    </div>
</div>
