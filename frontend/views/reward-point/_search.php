<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\RewardPoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reward-point-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reward_point_id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'reward_point') ?>

    <?= $form->field($model, 'equivalent_point') ?>

    <?= $form->field($model, 'datetime') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'order_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
