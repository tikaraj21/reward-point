<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RewardPoint */

$this->title = 'Create Reward Point';
$this->params['breadcrumbs'][] = ['label' => 'Reward Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reward-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
