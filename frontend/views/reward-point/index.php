<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RewardPoint */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reward Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reward-point-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reward Point', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'reward_point_id',
            'customer_id',
            'reward_point',
            'equivalent_point',
            'datetime',
            //'status',
            //'order_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
