<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RewardPoint */

$this->title = 'Update Reward Point: ' . $model->reward_point_id;
$this->params['breadcrumbs'][] = ['label' => 'Reward Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reward_point_id, 'url' => ['view', 'id' => $model->reward_point_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reward-point-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
