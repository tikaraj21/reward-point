-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2021 at 07:28 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rewardpoint`
--

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1637299016),
('m130524_201442_init', 1637299018),
('m190124_110200_add_verification_token_column_to_user_table', 1637299019);

-- --------------------------------------------------------

--
-- Table structure for table `rp_currency_equivalent`
--

CREATE TABLE `rp_currency_equivalent` (
  `currency_equivalent_id` int(11) NOT NULL,
  `currency_name` varchar(100) DEFAULT NULL,
  `currency_code` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `usd_equivalent` float(10,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp_orders`
--

CREATE TABLE `rp_orders` (
  `Order_ID` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `Order_Date` datetime DEFAULT NULL,
  `Sales_Type` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp_order_product`
--

CREATE TABLE `rp_order_product` (
  `Order_Product_ID` int(11) NOT NULL,
  `Order_ID` int(11) NOT NULL,
  `Item_Name` varchar(155) DEFAULT NULL,
  `Normal_Price` float(20,2) DEFAULT NULL,
  `Promotion_Price` float(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp_reward_point`
--

CREATE TABLE `rp_reward_point` (
  `reward_point_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `reward_point` float(20,2) DEFAULT NULL,
  `equivalent_point` float(20,2) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp_setting`
--

CREATE TABLE `rp_setting` (
  `setting_id` int(11) NOT NULL,
  `title` varchar(155) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rp_setting`
--

INSERT INTO `rp_setting` (`setting_id`, `title`, `code`, `value`) VALUES
(1, 'Currenct Name', 'CURRENCY', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `rp_user`
--

CREATE TABLE `rp_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rp_user`
--

INSERT INTO `rp_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'Cp8zPeEG29NeWzQH8OtnbEJNufA-rF3t', '$2y$13$dBbHh0GyMKVO7MgVm1nrG.Dby5IdaDBoc1/.7YtXozFmCPmG8jYbG', NULL, 'admin@gmail.com', 10, 1637299302, 1637299302, '5P-bHKUa7qo4kiqCXLuRrVHVzuPIabTc_1637299302');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `rp_currency_equivalent`
--
ALTER TABLE `rp_currency_equivalent`
  ADD PRIMARY KEY (`currency_equivalent_id`);

--
-- Indexes for table `rp_orders`
--
ALTER TABLE `rp_orders`
  ADD PRIMARY KEY (`Order_ID`);

--
-- Indexes for table `rp_order_product`
--
ALTER TABLE `rp_order_product`
  ADD PRIMARY KEY (`Order_Product_ID`),
  ADD KEY `Order_ID` (`Order_ID`);

--
-- Indexes for table `rp_reward_point`
--
ALTER TABLE `rp_reward_point`
  ADD PRIMARY KEY (`reward_point_id`);

--
-- Indexes for table `rp_setting`
--
ALTER TABLE `rp_setting`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `rp_user`
--
ALTER TABLE `rp_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rp_currency_equivalent`
--
ALTER TABLE `rp_currency_equivalent`
  MODIFY `currency_equivalent_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rp_orders`
--
ALTER TABLE `rp_orders`
  MODIFY `Order_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rp_order_product`
--
ALTER TABLE `rp_order_product`
  MODIFY `Order_Product_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rp_reward_point`
--
ALTER TABLE `rp_reward_point`
  MODIFY `reward_point_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rp_setting`
--
ALTER TABLE `rp_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rp_user`
--
ALTER TABLE `rp_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rp_order_product`
--
ALTER TABLE `rp_order_product`
  ADD CONSTRAINT `rp_order_product_ibfk_1` FOREIGN KEY (`Order_ID`) REFERENCES `rp_orders` (`Order_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
