common
    config/              contains shared configurations 
    models/              contains model classes used in both backend and frontend 

frontend
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    views/               contains view files for the Web application
vendor/                  contains dependent 3rd-party packages

RewardPoint
1. Project setup : 
	a. clone project and run command php init
	b. setup database in common/config/main-local.php
	c. import database from sql folder
2. Project flow
	a. Create Order: in frontend/controllers/OrdersController.php  actionCreateOrder() function get basket Total amount store in session and get customer reward point amount by getRewardPoint() function in RewardPoint model and getRewardPoint() function also update RewardPoint status to PayProcess.
	Get Reward Point from RewardPoint model (common/models/RewardPoint.php) with function getRewardPoint() which is calculate according to customer id and below basket total amount.
	
	b. After create order process completed or not if completed it goes to order controller function actionCompleteOrder(). This function updates Reward Point status to "Use" and calculate reward point in Order model function rewardCalculation().
	
	c. rewardCalculation: according to provided requirement calculate reward. It checks currency type, order expiry date(after 1yr), sales amount greater than or equals to $1. 
	In this function convert other currency amount to USD by usdConverter() function.
	
	d. usdConverter: according to database table data or also use api.
	
	e. Incomplete order - if Incomplete order update RewardPoint status from PayProcess to Collected.
	

